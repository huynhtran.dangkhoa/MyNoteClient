package com.example.dangkhoa.mynote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {
    public static String base_host = "http://mynoteeditor.herokuapp.com/api/v1/";
    TextView goToSignUp;
    EditText edtEmail, edtPassword;
    AppCompatButton btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        initialize();

        String checkLogedIn = settings.getString("Logedin", "");

        if (checkLogedIn.contains("true")){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivityForResult(intent, 0);
            finish();
        }
//        Toast.makeText(getApplicationContext(), checkLogedIn, Toast.LENGTH_SHORT).show();


        goToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUp = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(signUp, 0);
            }
        });

        edtEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode){
                        case KeyEvent.KEYCODE_ENTER: {
                            logIn(settings);
                            break;
                        }
                    }
                }
                return false;
            }
        });

        edtPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode){
                        case KeyEvent.KEYCODE_ENTER: {
                            logIn(settings);
                            break;
                        }
                    }
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn(settings);
            }
        });
    }

    void logIn(final SharedPreferences settings){
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String URL = "http://androidexample.com/media/webservice/httpget.php?user=\"+loginValue+\"&name=\"+fnameValue+\"&email=\"+emailValue+\"&pass=\"+passValue";

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(base_host + "login?email=" + edtEmail.getText() + "&password=" + edtPassword.getText(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
//                        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();

                if (!result.contains("Password incorrect.") && !result.contains("Error: Email does not exist.")){
                    SharedPreferences.Editor editor = settings.edit();


                    JSONObject jo = null;
                    try {
                        jo = new JSONObject(new String(responseBody));
                        editor.putString("email", String.valueOf(jo.get("email")));
                        editor.putString("name", String.valueOf(jo.get("name")));
                        editor.putString("Logedin", String.valueOf(true));
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivityForResult(intent, 0);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                }

                progressDialog.hide();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), new String(responseBody), Toast.LENGTH_SHORT).show();

                Log.i("Result", new String(responseBody));

                progressDialog.hide();
            }
        });
    }

    void initialize(){
        goToSignUp = (TextView) findViewById(R.id.link_signup);
        edtEmail = (EditText) findViewById(R.id.input_email);
        edtPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (AppCompatButton) findViewById(R.id.btn_login);
    }
}

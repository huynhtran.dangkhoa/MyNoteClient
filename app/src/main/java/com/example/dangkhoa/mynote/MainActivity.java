package com.example.dangkhoa.mynote;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.FragmentManager;
import android.preference.PreferenceManager;

import com.github.clans.fab.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dangkhoa.fragment.GroupsFragment;
import com.example.dangkhoa.fragment.HomeFragment;
import com.example.dangkhoa.fragment.TrashFragment;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView txtvEmail, txtvName;
    FloatingActionButton btnNote, btnVoice, btnCamera, btnHandWriting, btnSpeech;
    public static SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        settings = PreferenceManager.getDefaultSharedPreferences(this);


        String infoEmail = settings.getString("email", "");
        String infoName = settings.getString("name", "");

        initialize();
        txtvEmail.setText(infoEmail);
        txtvName.setText(infoName);

//        FloatingActionMenu fab = (FloatingActionMenu) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//            }
//        });

        btnNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateAndEditActivity.class);
                intent.putExtra("Mode", "Text");
                startActivityForResult(intent, 0);
            }
        });

        btnVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateAndEditActivity.class);
                intent.putExtra("Mode", "Voice");
                startActivityForResult(intent, 0);
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateAndEditActivity.class);
                intent.putExtra("Mode", "Camera");
                startActivityForResult(intent, 0);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
        setTitle("All Notes");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fm = getFragmentManager();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_home:{
                setTitle("All Notes");
                fm.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                break;
            }
            case R.id.nav_groups:{
                setTitle("Groups");
                fm.beginTransaction().replace(R.id.content_frame, new GroupsFragment()).commit();
                break;
            }
            case R.id.nav_trash:{
                setTitle("Trash");
                fm.beginTransaction().replace(R.id.content_frame, new TrashFragment()).commit();
                break;
            }
            case R.id.nav_logOut:{
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("Logedin", String.valueOf(false));
                editor.remove("email");
                editor.remove("name");
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, 0);
                finish();
                break;
            }
            case R.id.nav_about:{
                break;
            }
            case R.id.nav_help:{
                break;
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void initialize(){
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        txtvEmail = (TextView)header.findViewById(R.id.txtvInfoEmail);
        txtvName = (TextView)header.findViewById(R.id.txtvInfoName);

        btnNote = (FloatingActionButton) findViewById(R.id.menu_item_text);
        btnVoice = (FloatingActionButton)findViewById(R.id.menu_item_voice);
        btnCamera = (FloatingActionButton)findViewById(R.id.menu_item_camera);
        btnHandWriting = (FloatingActionButton)findViewById(R.id.menu_item_paint);
        btnSpeech = (FloatingActionButton)findViewById(R.id.menu_item_speech);
    }
}

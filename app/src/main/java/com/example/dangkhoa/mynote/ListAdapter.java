package com.example.dangkhoa.mynote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by dangkhoa on 03/01/2017.
 */

public class ListAdapter extends ArrayAdapter<Note> {

    public ListAdapter(Context context, int resource, List<Note> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
//            LayoutInflater inflater = LayoutInflater.from(getContext());
//            view =  inflater.inflate(R.layout.activity_line_note, false);

            view = LayoutInflater.from(getContext()).inflate(R.layout.activity_line_note, parent, false);
        }
        Note n = getItem(position);
        if (n != null) {
            // Anh xa + Gan gia tri
            TextView title = (TextView) view.findViewById(R.id.txtvNoteTitle);
            title.setText(n.Title);
            TextView content = (TextView) view.findViewById(R.id.txtvNoteContent);
            content.setText(n.Content);
            TextView id = (TextView) view.findViewById(R.id.txtvNoteID);
            id.setText(n.Id);
            TextView status = (TextView) view.findViewById(R.id.txtvNoteStatus);
            status.setText(n.Status);
        }
        return view;
    }

}

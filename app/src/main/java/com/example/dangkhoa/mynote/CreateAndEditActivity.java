package com.example.dangkhoa.mynote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;

public class CreateAndEditActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String cameraResult = "", voiceResult = "";
    String mode = "";

    ImageView imgImage;
    TextView txtvRecordFillename;
    EditText edtTitle, edtContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_edit);
        setTitle("");

        initialize();

        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String recordFilePath = settings.getString("RecordFilePath", "");

        if (recordFilePath.length() != 0){
            txtvRecordFillename.setVisibility(View.VISIBLE);
            txtvRecordFillename.setText(recordFilePath);
            voiceResult = FunctionClass.fileToBase64(recordFilePath);
        }else {
            txtvRecordFillename.setVisibility(View.GONE);
        }

        try {
            Intent i = getIntent();

            mode = i.getExtras().getString("Mode","");
        }catch (Exception e){

        }

        if (mode.contains("Voice")){
            startRecord();
        }else if (mode.contains("Camera")){
            openCamera();
        }else {

        }
//        Toast.makeText(getApplicationContext(), mode, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String infoEmail = MainActivity.settings.getString("email", "");

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("RecordFilePath");
        editor.commit();

        UpdateData(infoEmail, edtTitle.getText().toString(), edtContent.getText().toString(), cameraResult, voiceResult, "Draft");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar_create_and_edit, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mt_action_voice:{
                startRecord();
                break;
            }
            case R.id.mt_action_camera: {
                openCamera();
                break;
            }
            case R.id.mt_action_save: {
                String infoEmail = MainActivity.settings.getString("email", "");

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = settings.edit();
                editor.remove("RecordFilePath");

                editor.commit();

                UpdateData(infoEmail, edtTitle.getText().toString(), edtContent.getText().toString(), cameraResult, voiceResult, "Done");
                break;
            }

            default: {
                String infoEmail = MainActivity.settings.getString("email", "");

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = settings.edit();
                editor.remove("RecordFilePath");
                editor.commit();

                UpdateData(infoEmail, edtTitle.getText().toString(), edtContent.getText().toString(), cameraResult, voiceResult, "Draft");
                return super.onOptionsItemSelected(item);
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            Bitmap _imageBitmap = FunctionClass.getResizedBitmap(imageBitmap);
            cameraResult = FunctionClass.imageToBase64(imageBitmap);

            Uri uri = FunctionClass.getImageUri(getApplicationContext(), _imageBitmap);
            Picasso.with(getApplicationContext()).load(uri).into(imgImage);
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void startRecord(){
        Intent intent = new Intent(getApplicationContext(), RecordingActivity.class);
        startActivityForResult(intent, 0);
        finish();
    }

    public void UpdateData(String email, String title, String content, String image, String voice, String status){
//        final ProgressDialog progressDialog = new ProgressDialog(getApplicationContext(),
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCancelable(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Please wait...");
//        progressDialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(LoginActivity.base_host + "create?email=" + email + "&title=" + title + "&content=" + content + "&image=" + Uri.encode(image) + "&voice=" + Uri.encode(voice) + "&status=" + status, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

//                progressDialog.hide();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String result = new String(responseBody);
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

//                progressDialog.hide();
            }
        });
    }

    public void initialize(){
        imgImage = (ImageView) findViewById(R.id.img_image_note);
        txtvRecordFillename = (TextView) findViewById(R.id.txtv_record_filename);
        edtTitle = (EditText) findViewById(R.id.edt_note_titlle);
        edtContent = (EditText) findViewById(R.id.edt_note_content);
    }
}
